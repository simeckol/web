<?php

/**
 * Class Register
 * Controller pro registraci
 */
class ConRegister
{

    public function __construct()
    {
    }

    /**
     *  Vrati obsah stranky
     * @return string Obsah stranky
     */
    public function getResult()
    {
        $actionResult = null; // informace o vysledku mazani
        if(isset($_POST['submit'])){
            // objekt pro ziskani dat
            include("mod-databaze.class.php");
            // vytvorim si konkretni objekt
            $db = new ModDatabaze;
            $actionResult = null; // informace o vysledku
            // smazu daneho uzivatele
            $db->addUser($_POST['jmeno'], $_POST['email'], $_POST['heslo']);
            $actionResult = "Uživatel $_POST[jmeno] byl registrován, nyní se můžete přihlásit";
        }

        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();

        // objekt pro vytvoreni sablony
        include("view-register.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewRegister::getTemplate($actionResult, $isLogged, $name, $role);
        // vratim vysledny vzhled webu
        return $html;
    }

    /**
     * Zpracování formuláře
     */
//    public function submit(){
//        include("mod-databaze.class.php");
//        $db = new ModDatabaze();
//        if(isset($_POST)){
//            $nick = $_POST['nick'];
//            $email = $_POST['email'];
//            $pass = $_POST['pwd'];
//            $pass2 = $_POST['pwd2'];
//
//            if($db->isNickTaken($nick)){
//                $this->error('uživatelské jméno již existuje');
//            }
//            else{
//                if($pass == $pass2){
//                    if(strlen($pass)<4){
//                        $this->error('Heslo musí mít alespoň 4 znaky.');
//                    }
//                    else{
//                        $db->addUser($nick, $email, $pass);
//                        $this->success('Byl jste úspěšně zaregistrován, nyní se můžete přihlásit.');
//                    }
//                }
//                else{
//                    $this->error('Hesla se neshodují');
//                }
//            }
//
//        }
//        else{
//            $this->error('Chyba při odesílání formuláře');
//        }
//    }

    public function submit($log, $pas, $mail)
    {
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        $db->addUser($log, $pas, $mail);
        return $db->login($log, $pas);
    }
}

?>