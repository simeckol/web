<?php

class ViewPostsToAcception
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($data, $isLogged, $name, $role)
    {
        $res = "<div class='col-md-10 col-sm-4'>";
        // projdu data
        foreach ($data as $d) {
            $res .= "<h2>$d[title]</h2>";
            $res .= "Autor: $d[autors] (" . date("d. m. Y", strtotime($d['date'])) . ")<br><br>";
            $res .= "<div style='text-align:justify;'>Úryvek: $d[content]</div>";

            $res .= "<form method='post' action='con-index.php?web=acceptation'>
                        <input type='hidden' name='id' value='$d[id]'>
                        <input class='btn btn-success conf-button' type='submit' name='view' value='Zobrazit hodnocení a rozhodnout o přijetí'>
                    </form>";

            $res .= "<hr>";

        }

        $res .= "</div>";
//        $res = $data;

        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Články ke schválení", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }

}

?>