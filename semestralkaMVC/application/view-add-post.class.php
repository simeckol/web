<?php

class ViewAddPost
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($actionResult, $isLogged, $name, $role)
    {
        $res = "";

        if ($actionResult != null) {
            $res .= "<div class='alert alert-success' role='alert'>
                        <strong>$actionResult</strong>
                     </div>";
        }

        $res .= "<form  enctype='multipart/form-data' action='con-index.php?web=add-post' method='post'>
                <fieldset>
                    <legend>Nový článek</legend>
                    Nadpis: <input type='text' name='title' maxlength='60' required><br>
                    <textarea class='conf-textarea' type='text' name='text' placeholder='zadejte Váš text' required></textarea><br>
                    <label class='label' for=\"file\">PDF soubor:</label>
                    <input class='file upload' id='file' type='file' name='file' required>
                    <input class='btn btn-success' type='submit' name='submit' value='Odeslat příspěvek'>
                </fieldset>
            </form>";
        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Přidat článek", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }

}

?>