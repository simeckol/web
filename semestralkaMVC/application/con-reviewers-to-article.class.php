<?php

class ConReviewersToArticle {
    
    public function __construct() {

    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){

        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();
        $id = $loginCon->getUserId();
        $title = null;
        $content = null;

        // objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;

        $reviewers = $db->getReviewers();
        $data = $db->getReviewersOfArticle($_POST['id_article']);

        if (isset($_POST['view'])) {
            $title = $_POST['title'];
            $content = $_POST['content'];
        }

//        $data = $loginCon->getUserInfo();
        //print_r($data);
        // objekt pro vytvoreni sablony
        include("view-reviewers-to-article.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewReviewersToArticle::getTemplate($reviewers, $title, $content, $data, $_POST['id_article'], $isLogged, $name, $role);
        // vratim vysledny vzhled webu
        return $html;
    }
        
}

?>