

<?php

class ViewPostsToReview
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($data, $isLogged, $name, $role)
    {
        $res = "";
        // projdu data
        foreach ($data as $d) {
            $res .= "<h2>$d[title]</h2>";
            $res .= "Autor: $d[autors] (" . date("d. m. Y", strtotime($d['date'])) . ")<br><br>";
            $res .= "<div style='text-align:justify;'>Úryvek: $d[content]</div>";

            if ($d['accepted'] == 0) {
                $res .= "<form method='post' action='con-index.php?web=posts-to-review'>
                
                        <input type='hidden' name='id' value='$d[id]'>";
            }

            $res .= "Nápad: $d[idea]";

            if ($d['lock_edit'] == 0) {
                $res .= "<select name='idea'>
                            <option value='$d[idea]' selected disabled>Vyberte zde</option>
                            <option value=\"1\">Nedoporučuji</option>
                            <option value=\"2\">Podprůměr</option>
                            <option value=\"3\">Průměr</option>
                            <option value=\"4\">Nadprůměr</option>
                            <option value=\"5\">Výborné</option>
                        </select>";
            }


            $res .= "Téma: $d[theme]";

            if ($d['lock_edit'] == 0) {
                $res .= "<select name='theme'>
                        <option value='$d[theme]' selected disabled>Vyberte zde</option>
  <option value=\"1\">Nedoporučuji</option>
  <option value=\"2\">Podprůměr</option>
  <option value=\"3\">Průměr</option>
  <option value=\"4\">Nadprůměr</option>
  <option value=\"5\">Výborné</option>
</select>";
            }


            $res .= "Komentář:";

            if ($d['lock_edit'] == 1) {
                $res .= "$d[note]";
            }


            if ($d['lock_edit'] == 0) {
                $res .= "<textarea type='text' name='text' placeholder='zadejte Váš text'>$d[note]</textarea>

                        <input class='btn btn-success' type='submit' name='submit' value='Změnit'>
                    </form><hr>";
            }

        }
//        $res = $data;

        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Recenzování", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }

}

?>