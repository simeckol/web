<?php

class ViewSpravaUzivatelu {
    
    public function __construct() {
        
    }

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     *  @param array $data Data pro zobrazeni.
     *  @param string $actionResult Vysledek operace (mazani).
     *  @return string Vysledny vzhled.
     */
    public static function getTemplate($data, $actionResult=null, $isLogged, $name, $role){
        $res = "<div class='col-md-8 col-sm-4'>";
        // mam hlasku pro vypsani
        if($actionResult!=null){
            $res .= "<div class=\"alert alert-success\" role=\"alert\">
        <strong>$actionResult</strong>
    </div>";
        }
        // zde bude vysledne zobrazeni
        $res .= "<table class='table table-bordered conf-table'>";
        $res .= "<tr><th>ID</th><th>Login</th><th>E-mail</th><th>Role</th><th>Změnit roli</th><th>Smazat</th></tr>";
        // projdu data
        foreach($data as $d){
            $res .= "<tr>";
            // vypis dat
            $res .= "<td>$d[id_user]</td><td>$d[nick]</td><td>$d[email]</td>";
//            urceni role
            $res .= "<td><form method='post' action='con-index.php?web=sprava'>";
if ($d['role'] == 1){
    $res .= "Admin";
}else if ($d['role'] == 2) {
    $res .= "Autor";
} else if ($d['role'] == 3) {
    $res .= "Recenzent";
} else {
    $res .= "CHYBA!";
}
                        $res .= "</td><td><input type='hidden' name='userId' value='$d[id_user]'>
                        <select name='roles'>
                        <option selected disabled>Vyberte zde</option>
  <option value='1'>Admin</option>
  <option value='2'>Autor</option>
  <option value='3'>Recenzent</option>
</select>
                        <input class='btn btn-warning' type='submit' name='zmenit-roli' value='Změnit'>
                    </form></td>";
            // form. pro smazani uzivatele
            $res .= "<td><form method='post' action='con-index.php?web=sprava'>
                        <input type='hidden' name='userId' value='$d[id_user]'>
                        <input class='btn btn-danger' type='submit' name='submit' value='smazat'>
                    </form></td>";
            $res .= "</tr>";
        }
        $res .= "</table>";

        $res .= "<div>";

        // doplnim data hlavicky
        include("view-header.class.php");
        include ("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Správa uživatelů", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }
    
}

?>