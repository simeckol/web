<?php

class ViewHome {
    
    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     *  @param array $data Data pro zobrazeni. 
     *  @return string Vysledny vzhled.
     */
    public static function getTemplate($data, $isLogged, $name, $role){
//        $res = "";
//        // projdu data
//        foreach($data as $d){
//            $res .= "<h2>$d[title]</h2>";
//            $res .= "Autor: $d[autors] (".date("d. m. Y, H:i.s", strtotime($d['date'])).")<br><br>";
//            $res .= "<div style='text-align:justify;'>Úryvek: $d[content]</div><hr>";
//        }
        $res = $data;

        // doplnim data hlavicky
        include("view-header.class.php");
        include ("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Vítejte!", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }
    
}

?>