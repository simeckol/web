<?php

class ViewAcceptation
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($data, $id, $isLogged, $name, $role)
    {
        $res = "";
        // zde bude vysledne zobrazeni

        // projdu data
        if ($data != null) {
            $res .= "<table class='table table-bordered'>";
            $res .= "<tr><th>Nápad</th><th>Téma</th><th>Poznámka</th></tr>";
            foreach($data as $d){
                $res .= "<tr>";
                // vypis dat
                $res .= "<td>$d[idea]</td><td>$d[theme]</td><td>$d[note]</td>";
                $res .= "</tr>";
            }
            $res .= "</table>";
        } else {
            $res .= "<div class='alert alert-danger message' role='alert'>
                        <strong>Nebyla nalezena žádná hodnocení.</strong>
                     </div>";
        }


        $res .= "<form method='post' action='con-index.php?web=posts-to-acception'>
                        <input type='hidden' name='id' value='$id'>
                        <input class='btn btn-success conf-button' type='submit' name='accept' value='Přijmout'>
                    </form>";

        $res .= "<form method='post' action='con-index.php?web=posts-to-acception'>
                        <input type='hidden' name='id' value='$id'>
                        <input class='btn btn-danger conf-button' type='submit' name='disaccept' value='Zamítnout'>
                    </form>";
//        $res = $data;

        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Rozhodnout o přijetí článku", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }
}

?>