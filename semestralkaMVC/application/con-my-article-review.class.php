<?php

class ConMyArticleReview {
    
    public function __construct() {

    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){


// objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        // ziskam data
        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();
        $id = $loginCon->getUserId();
        $data = null;
//        $data = $loginCon->getUserInfo();

        $html = null;
        include("view-my-article-review.class.php");

        if (isset($_POST['hodnoceni']) && isset($_POST['id'])) {
            $data = $db->getReviewsByArticle($_POST['id']);
        }
            $html = ViewMyArticleReview::getTemplate($data, $_POST['title'], $_POST['content'], $isLogged, $name, $role);

        return $html;

    }
        
}

?>