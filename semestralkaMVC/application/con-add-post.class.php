<?php

class ConAddPost
{

    public function __construct()
    {

    }

    /**
     *  Vrati obsah stranky
     * @return string Obsah stranky
     */
    public function getResult()
    {

        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();
        $id = $loginCon->getUserId();


        // objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;

        $actionResult = null;


        if (isset($_POST['submit']) && isset($_POST['title']) && isset($_POST['text'])) {
            $db->addPost($_POST['title'], $name, $_POST['text'], $id, date("Y-m-d"));

            $target_path = "doc/";

            $target_path = $target_path . basename( $_FILES['file']['name']);

            if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
                $actionResult = "Článek $_POST[title] se souborem ".  basename( $_FILES['file']['name']).
                    " byl úspěšně nahrán. Článek a jeho aktuální stav si můžete prohlédnout v záložce Mé články.";
            } else{
                $actionResult = "Během nahrávání souboru došlo k chybě, zkuste to, prosím, znovu!";
            }
        }

        // objekt pro vytvoreni sablony
        include("view-add-post.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewAddPost::getTemplate($actionResult, $isLogged, $name, $role);
        // vratim vysledny vzhled webu
        return $html;
    }

}

?>