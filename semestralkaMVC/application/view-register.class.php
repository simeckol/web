<?php

class ViewRegister
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($actionResult = null, $isLogged, $name, $role)
    {
        $res = "";

        if ($actionResult != null) {
            $res .= "<div class='alert alert-success message' role='alert'>
                        <strong>$actionResult</strong>
                     </div>";
        }

        // projdu data
        $res .= "<form method='post' action=''>
        Jméno<br />
        <input type='text' name='jmeno' required/><br />
        E-mail<br />
        <input type='text' name='email' required/><br />
        Heslo<br />
        <input type='password' name='heslo' required/><br />
        <input class='btn btn-success conf-button' type='submit' name='submit' value='Registrovat' />
</form>";
        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Registrace", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }

}

?>
