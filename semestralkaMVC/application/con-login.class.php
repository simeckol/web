<?php 

class ConLogin{

    private $ses; // objekt MySession
    private $dName = "jmeno"; // nazev sessny pro jmeno
    private $dDate = "datum"; // nazev sessny pro datum
    private $dRole = "role"; // nazev sessny pro roli
    private $dId = "id"; // nazev sessny pro id

    /**
     *  Pri vytvoreni objektu zahaji session.
     */
    public function __construct(){
        include_once("session.class.php");
        // inicializuju objekt sessny
        $this->ses = new Session();
        // spusti session pro spravu prihlaseni uzivatele
//        session_start();
//        // importuje funkce pro práci s databází
//        include_once("mod-databaze.class.php");
//        $this->db = new ModDatabaze();
    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult() {
        // ziskam data
//        $data = $this->getUserInfo();

        $actionResult = null;
        if (isset($_POST['login']) && isset($_POST['jmeno']) && isset($_POST['heslo'])) {
            $actionResult = $this->login($_POST['jmeno'], $_POST['heslo']);
        } else if (isset($_POST['logout'])) {
            $this->logout();
            $actionResult = "Uživatel byl odhlášen.";
        }

        $name = $this->getUserName();
        $role = $this->getUserRole();
        $isLogged = $this->isUserLoged();

        if ($isLogged) {
            $actionResult = "Zde se můžete odhlásit.";
        }

        //print_r($data);
        // objekt pro vytvoreni sablony
        include("view-login.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewLogin::getTemplate($actionResult, $isLogged, $name, $role);
        // vratim vysledny vzhled webu
        return $html;
    }

    /**
     *  Otestuje, zda je uzivatel prihlasen
     *  @return boolean
     */
    public function isUserLoged(){
        return $this->ses->isSessionSet($this->dName);
    }

    /**
     *  Nastavi do session jmeno uzivatele a datum prihlaseni.
     *  @param string $userName Jmeno uzivatele.
     */
    public function login($log, $pas){

        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        $login = $db->getUser($log, $pas);
//        $array = (array)$login;

        if(count($login) > 0) {

            $this->ses->addSession($this->dName, $login[0]['nick']); // jmeno
            $this->ses->addSession($this->dDate, date("Y-m-d"));
            $this->ses->addSession($this->dRole, $login[0]['role']);
            $this->ses->addSession($this->dId, $login[0]['id_user']);

            return "user was logged in";

        } else {
            return "an error occurred";
        }
    }

    /**
     *  Odhlasi uzivatele.
     */
    public function logout(){
        $this->ses->removeSession($this->dName);
        $this->ses->removeSession($this->dDate);
        $this->ses->removeSession($this->dRole);
        $this->ses->removeSession($this->dId);
    }

    /**
     *  Vrati informace o uzivateli
     *  @return string Informace o uzivateli.
     */
    public function getUserName(){
        $name = $this->ses->readSession($this->dName);
        return $name;
    }

    public function getUserRole(){
        $role = $this->ses->readSession($this->dRole);
        return $role;
    }

    public function getUserId(){
        $id = $this->ses->readSession($this->dId);
        return $id;
    }
}

?>