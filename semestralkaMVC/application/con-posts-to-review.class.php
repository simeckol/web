<?php

class ConPostsToReview {
    
    public function __construct() {

    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){
        // objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        // ziskam data

        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $id = $loginCon->getUserId();
        $isLogged = $loginCon->isUserLoged();

        if (isset($_POST['submit']) && isset($_POST['id']) && (isset($_POST['idea']) || isset($_POST['theme']) || isset($_POST['text']))){

            if (isset($_POST['idea'])) {
                $db->reviewPostByIdea($_POST['id'], $id, $_POST['idea']);
            }

            if (isset($_POST['theme'])) {
                $db->reviewPostByTheme($_POST['id'], $id, $_POST['theme']);
            }

            if (isset($_POST['text'])) {
                $db->reviewPostByNote($_POST['id'], $id, $_POST['text']);
            }

//            $db->reviewPost($_POST['id'], $id, $idea, $theme, $text);
        }

        $data = $db->getUserAssignedPosts($id);
//        $data = $loginCon->getUserInfo();
        //print_r($data);
        // objekt pro vytvoreni sablony
        include("view-posts-to-review.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewPostsToReview::getTemplate($data, $isLogged, $name, $role);
        // vratim vysledny vzhled webu
        return $html;
    }
        
}

?>