<?php

class ConPosts {
    
    public function __construct() {

    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){
        // objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        // ziskam data
        $data = $db->getAcceptedPosts();
        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();

//        $data = $loginCon->getUserInfo();
        //print_r($data);
        // objekt pro vytvoreni sablony
        include("view-posts.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewPosts::getTemplate($data, $isLogged, $name, $role);
        // vratim vysledny vzhled webu
        return $html;
    }
        
}

?>