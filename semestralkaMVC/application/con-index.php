<?php 
    
    // dostupne stranky
    $possible = array("home", "sprava", "registrace", "login", "posts", "add-post", "my-posts", "posts-to-review",
        "my-article-review", "edit-post", "posts-to-review", "posts-to-acception", "acceptation", "set-reviewers",
        "reviewers-to-article");
    // kontrolery danych stranek
    $controllers = array("con-home.class.php", "con-sprava-uzivatelu.class.php", "con-register.class.php",
        "con-login.class.php", "con-posts.class.php", "con-add-post.class.php", "con-my-posts.class.php",
        "con-posts-to-review.class.php", "con-my-article-review.class.php", "con-edit-post.class.php",
        "con-posts-to-review.class.php", "con-posts-to-acception.class.php", "con-acceptation.class.php",
        "con-set-reviewers.class.php", "con-reviewers-to-article.class.php");
    // objekty danych kontroleru
    $objects = array("ConHome", "ConSpravaUzivatelu", "ConRegister", "ConLogin", "ConPosts", "ConAddPost", "ConMyPosts",
        "ConPostsToReview", "ConMyArticleReview", "ConEditPost", "ConPostsToReview", "ConPostsToAcception",
        "ConAcceptation", "ConSetReviewers", "ConReviewersToArticle");

    // klasicke URL
    if(isset($_GET["web"]) && in_array($_GET["web"], $possible)){
        $input = $_GET["web"];
    } else {
        $input = "home";
    }
    
    // hezke URL - jen mozny nastin - nutno dopracovat
    //$url=strip_tags($_SERVER['REQUEST_URI']);
    //$urlAr=explode("/",$url); 
    //echo $url;

    // mam vstup?
    if(isset($input)){
        // ziskam index stranky
        $i = array_search($input, $possible);
        // ziskam spravnou stranku
        $web = $controllers[$i];
        // includuji
        include($web);
        // vytvorim odpovidajici kontroler
        $con = new $objects[$i];
        // ziskam vysledek kontroleru
        $result = $con->getResult();
        // vypisu vysledek, tj. zobrazim web
        echo $result;
    } else {
        // vstup neni spravny - pouze vypisu mini HTML
        echo "<html><head><meta charset='utf-8'></head><body>stránka není dostupná</body></html>";
    }
    


?>