<?php

class ViewMyArticleReview
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($data, $title, $content, $isLogged, $name, $role)
    {
        $res = "";
        // zde bude vysledne zobrazeni


        $res .= "<h2>$title</h2>";
        $res .= "<div style='text-align:justify;'>Úryvek: $content</div><br><br>";

        if ($data != null) {
            $res .= "<table class='table table-bordered'>";
            $res .= "<tr><th>Nápad</th><th>Téma</th><th>Poznámka</th></tr>";

            // projdu data
            foreach ($data as $d) {
                $res .= "<tr>";
                // vypis dat

                $res .= "<td>";
                if ($d['idea'] == 1) {
                    $res .= "Nedoporučuji";
                } else if ($d['idea'] == 2) {
                    $res .= "Podprůměr";
                } else if ($d['idea'] == 3) {
                    $res .= "Průměr";
                } else if ($d['idea'] == 4) {
                    $res .= "";
                } else if ($d['idea'] == 5) {
                    $res .= "";
                }
                $res .= "</td>";

                $res .= "<td>";
                if ($d['theme'] == 1) {
                    $res .= "Nedoporučuji";
                } else if ($d['theme'] == 2) {
                    $res .= "Podprůměr";
                } else if ($d['theme'] == 3) {
                    $res .= "Průměr";
                } else if ($d['theme'] == 4) {
                    $res .= "";
                } else if ($d['theme'] == 5) {
                    $res .= "";
                }
                $res .= "</td>";

                $res .= "<td>$d[note]</td>";
                $res .= "</tr>";
            }
            $res .= "</table>";
        } else {
            $res .= "<div class='alert alert-danger message' role='alert'>
                        <strong>Nebyla nalezena žádná hodnocení.</strong>
                     </div>";
        }
//        $res = $data;

        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Hodnocení mého článku", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }

}

?>