<?php

class ConMyPosts {
    
    public function __construct() {

    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){


// objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        // ziskam data
        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();
        $id = $loginCon->getUserId();
//        $data = $loginCon->getUserInfo();
        $actionResult = null;

        $html = null;
        include("view-my-posts.class.php");

        if (isset($_POST['smazat']) && isset($_POST['id'])) {
            $db->deletePost($_POST['id']);
            $actionResult = "Článek $_POST[title] s ID $_POST[id] byl úspěšně smazán.";
        } else if (isset($_POST['upravit-second']) && isset($_POST['id']) && isset($_POST['content']) ) {
            $db->editPost($_POST['id'], $_POST['content']);
        }

        $data = $db->getPostsByUser($id);
        $html = ViewMyPosts::getTemplate($data, $actionResult, $isLogged, $name, $role);



        return $html;

    }
        
}

?>