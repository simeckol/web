<?php

class ViewEditPost
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($isLogged, $name, $role, $content, $title, $id)
    {
        $res = "<form action='con-index.php?web=my-posts' method='post'>
                <fieldset>
                    <legend>Upravit článek $title</legend>
                    <input type='hidden' name='id' value='$id'>
                    Původní text: $content
                    <textarea class='conf-textarea' type='text' name='content'>$content</textarea><br>
                    <input class='btn btn-success' type='submit' name='upravit-second' value='Upravit'>
                </fieldset>
            </form>";
        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Upravit clanek", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }

}

?>