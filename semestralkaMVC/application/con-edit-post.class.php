<?php

class ConEditPost {
    
    public function __construct() {

    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){

        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();
        $id = $loginCon->getUserId();


        // objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        $content = null;
        $title = null;

        if (isset($_POST['upravit-first']) && isset($_POST['id']) && isset($_POST['content']) && isset($_POST['title'])) {
            $content = $_POST['content'];
            $title = $_POST['title'];
        }
//        $data = $loginCon->getUserInfo();
        //print_r($data);
        // objekt pro vytvoreni sablony
        include("view-edit-post.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewEditPost::getTemplate($isLogged, $name, $role, $content, $title, $_POST['id']);
        // vratim vysledny vzhled webu
        return $html;
    }
        
}

?>