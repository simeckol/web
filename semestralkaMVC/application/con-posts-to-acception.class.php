<?php

class ConPostsToAcception {
    
    public function __construct() {

    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){


// objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        // ziskam data
        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();
        $id = $loginCon->getUserId();
//        $data = $loginCon->getUserInfo();

        $html = null;
        include("view-posts-to-acception.class.php");

        if (isset($_POST['accept']) && isset($_POST['id'])) {
            $db->approvePost($_POST['id']);
            $db->lockPost($_POST['id']);
        }

        if (isset($_POST['disaccept']) && isset($_POST['id'])) {
            $db->disapprovePost($_POST['id']);
            $db->lockPost($_POST['id']);
        }

        $data = $db->getUnacceptedPosts();
        $html = ViewPostsToAcception::getTemplate($data, $isLogged, $name, $role);

        return $html;

    }
        
}

?>