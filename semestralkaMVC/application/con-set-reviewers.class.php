<?php

class ConSetReviewers {
    
    public function __construct() {

    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){
        // objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        // ziskam data

        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();
        $reviewers = $db->getReviewers();

        if (isset($_POST['set'])) {
            foreach ($reviewers as $r){
                if ($this->IsChecked('check', $r['id_user'])){
                    if (!$db->isReviewAssigned($_POST['id_article'], $r['id_user'])){
                        $db->assignPost($_POST['id_article'], $r['id_user']);
                    }
                } else {
                    $db->deleteAssignment($_POST['id_article'], $r['id_user']);
                }
            }
        }

        $data = $db->getUnacceptedPosts();
//        $data = $loginCon->getUserInfo();
        //print_r($data);
        // objekt pro vytvoreni sablony
        include("view-set-reviewers.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewSetReviewers::getTemplate($data, $isLogged, $name, $role);
        // vratim vysledny vzhled webu
        return $html;
    }

    function IsChecked($chkname, $value)
    {
        if(!empty($_POST[$chkname]))
        {
            foreach($_POST[$chkname] as $chkval)
            {
                if($chkval == $value)
                {
                    return true;
                }
            }
        }
        return false;
    }
        
}

?>