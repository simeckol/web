<?php

class ViewSetReviewers {
    
    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     *  @param array $data Data pro zobrazeni. 
     *  @return string Vysledny vzhled.
     */
    public static function getTemplate($data, $isLogged, $name, $role){
        $res = "";
        // projdu data
        foreach($data as $d){
            $res .= "<h2>$d[title]</h2>";
            $res .= "Autor: $d[autors] (".date("d. m. Y", strtotime($d['date'])).")<br><br>";
            $res .= "<div style='text-align:justify;'>Úryvek: $d[content]</div>";
            $res .= "<form method='post' action='con-index.php?web=reviewers-to-article'>
                        <input type='hidden' name='id_article' value='$d[id]'>
                        <input type='hidden' name='title' value='$d[title]'>
                        <input type='hidden' name='content' value='$d[content]'>
                        <input class='btn btn-success conf-button' type='submit' name='view' value='Upravit recenzenty'>
                    </form><hr>";
        }
//        $res = $data;

        // doplnim data hlavicky
        include("view-header.class.php");
        include ("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Přiřadit recenzenty", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }
    
}

?>