<?php

class ViewReviewersToArticle
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($reviewers, $title, $content, $data, $id, $isLogged, $name, $role)
    {
        $res = "<h2>$title</h2>";
        $res .= "<div style='text-align:justify;'>$content</div><br><br>";

        $res .= "<form action='con-index.php?web=set-reviewers' method='post'>";
        $res .= "<input type='hidden' name='id_article' value='$id'>";
        foreach ($reviewers as $r) {
            $res .= "<input type='checkbox' name='check[]' ";

            foreach($data as $d) {
                if ($d['id_user'] == $r['id_user']) {
                    $res .= " checked ";
                }
            }

            $res .= " value='$r[id_user]'>$r[nick]<br>";
        }
        $res .= "<input class='btn btn-success conf-button' type='submit' name='set' value='Změnit'>
            </form>";

        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Upravit recenzenty", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }



}

?>