<?php

class ViewMyPosts
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($data, $actionResult, $isLogged, $name, $role)
    {
        $res = "";

        if($actionResult!=null){
            $res .= "<div class='alert alert-success' role='alert'>
                        <strong>$actionResult</strong>
                    </div>";
        }

        // projdu data
        foreach ($data as $d) {
            $res .= "<h2>$d[title]</h2>";
            $res .= "Autor: $d[autors] (" . date("d. m. Y", strtotime($d['date'])) . ")<br><br>";
            $res .= "<div class='conf-cont'>Úryvek: $d[content]</div><br>";
            if ($d['accepted'] == 1) {
                $res .= "<div class='alert alert-success message' role='alert'>
                        <strong>Článek byl přijat.</strong>
                     </div>";
                $res .= "<form method='post' class='inner-button' action='con-index.php?web=my-article-review'>
                        <input type='hidden' name='id' value='$d[id]'>
                        <input type='hidden' name='title' value='$d[title]'>
                        <input type='hidden' name='content' value='$d[content]'>
                        <input class='btn btn-success' type='submit' name='hodnoceni' value='Zobrazit hodnocení'>
                    </form>";
            } else if ($d['accepted'] == -1) {
                $res .= "<div class='alert alert-danger message' role='alert'>
                        <strong>Článek byl zamítnut.</strong>
                     </div>";
                $res .= "<form method='post' class='inner-button' action='con-index.php?web=my-article-review'>
                        <input type='hidden' name='id' value='$d[id]'>
                        <input type='hidden' name='title' value='$d[title]'>
                        <input type='hidden' name='content' value='$d[content]'>
                        <input class='btn btn-success' type='submit' name='hodnoceni' value='Zobrazit hodnocení'>
                    </form>";
            } else {
                $res .= "<div class='alert alert-info message' role='alert'>
                        <strong>Článek zatím nebyl přijat.</strong>
                     </div>";
            }

            $res .= "<form method='post' class='inner-button' action='con-index.php?web=edit-post'>
                        <input type='hidden' name='id' value='$d[id]'>
                        <input type='hidden' name='title' value='$d[title]'>
                        <input type='hidden' name='content' value='$d[content]'>
                        <input class='btn btn-success' type='submit' name='upravit-first' value='Upravit'>
                    </form>";
            $res .= "<form method='post' action='con-index.php?web=my-posts'>
                        <input type='hidden' name='id' value='$d[id]'>
                        <input type='hidden' name='title' value='$d[title]'>
                        <input class='btn btn-danger' type='submit' name='smazat' value='Smazat'>
                    </form><br>";

            $res .= "<hr>";

        }
//        $res = $data;

        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Mé články", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }

}

?>