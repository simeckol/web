<?php

class ConSpravaUzivatelu {
    
    public function __construct() {
        
    }

    /**
     *  Vrati obsah stranky
     *  @return string Obsah stranky
     */
    public function getResult(){
        // objekt pro ziskani dat
        include("mod-databaze.class.php");
        $db = new ModDatabaze;
        // nemam smazat uzivatele?
        $actionResult = null; // informace o vysledku mazani
        if(isset($_POST['submit']) && isset($_POST["userId"])){
            // smazu daneho uzivatele
            $db->deleteUser($_POST["userId"]);
            $actionResult = "Uživatel s ID $_POST[userId] byl smazán.";
        } else if(isset($_POST['zmenit-roli']) && isset($_POST['userId']) && isset($_POST['roles'])) {
            $db->editUser($_POST['userId'], $_POST['roles']);
        }

        include "con-login.class.php";
        $loginCon = new ConLogin();
        $name = $loginCon->getUserName();
        $role = $loginCon->getUserRole();
        $isLogged = $loginCon->isUserLoged();
        
        // ziskam data vsech uzivatelu
        $data = $db->getUsers();
        //print_r($data);
        // objekt pro vytvoreni sablony
        include("view-sprava-uzivatelu.class.php");
        // predam data sablone a ziskam jejich vizualizaci
        $html = ViewSpravaUzivatelu::getTemplate($data, $actionResult, $isLogged, $name, $role);
        // vratim vysledny vzhled webu
        return $html;
    }
    
}

?>