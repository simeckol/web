<?php

class ViewHeader {
    
    
    /**
     *  Vrati header.
     *  @param string $title Nazev stranky.
     *  @return string header.
     */
    public static function getHTMLHeader($title, $isLogged, $name, $role){
        $res = "";

        $res .= "<html lang='cs'>
<head>
    <meta charset='utf-8'>
    <!-- Latest compiled and minified CSS -->
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' crossorigin='anonymous'>

    <!-- Optional theme -->
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css' integrity='sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp' crossorigin='anonymous'>

    <!-- Latest compiled and minified JavaScript -->
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' integrity='sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa' crossorigin='anonymous'></script>
    <link rel='stylesheet' href='styles.css'>
    <title>Semestrálka KIV/WEB</title>
</head>
<nav class='navbar navbar-inverse navbar-fixed-top'>
    <div class='container'>
        <div class='navbar-header'>
            <a class='navbar-brand' href='con-index.php?web=uvod'>Konference</a>
        </div>
   <div id='navbar' class='navbar-collapse collapse'>
            <ul class='nav navbar-nav'>
                <li><a href='con-index.php?web=uvod'>Domů</a></li>
                <li><a href='con-index.php?web=posts'>Články</a></li>";

        if ($role == 1) { //admin
            $res .= "<li><a href='con-index.php?web=sprava'>Správa uživatelů</a></li>";
            $res .= "<li><a href='con-index.php?web=posts-to-acception'>Články ke schválení</a></li>";
            $res .= "<li><a href='con-index.php?web=set-reviewers'>Přiřazení recenzentů</a></li>";
        } elseif ($role == 2) { //autor
            $res .= "<li><a href='con-index.php?web=add-post'>Nový článek</a></li>";
            $res .= "<li><a href='con-index.php?web=my-posts'>Mé články</a></li>";
        } elseif ($role == 3) { //recenzent
            $res .= "<li><a href='con-index.php?web=posts-to-review'>Články k hodnocení</a></li>";
        }
        if (isset($name)) {
            $res .= " <li><a href='#'><aman><span class='glyphicon glyphicon-user'></span></aman> <aman>$name</aman></a></li>";
        }


        $res .= "</ul>";

        $res .= "<ul class='nav navbar-nav navbar-right'>";



        if ($isLogged) {
            $res .= "<li><a href='con-index.php?web=login'><aman><span class='glyphicon glyphicon-log-out'></span></aman> <aman>Odhlášení</aman></a></li>";
        } elseif (!$isLogged) {
            $res .= "<li><a href='con-index.php?web=login'><aman><span class='glyphicon glyphicon-log-in'></span></aman> <aman>Přihlášení</aman></a></li>
<li><a href='con-index.php?web=registrace'><aman><span class='glyphicon glyphicon-user'></span></aman> <aman>Registrace</aman></a></li>";
        }

        $res .= "</ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class='container theme-showcase' role='main'>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class='jumbotron'>
        <h1>$title</h1>
    </div>";
        return $res;
    }

}

?>