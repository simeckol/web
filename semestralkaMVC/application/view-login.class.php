<?php

class ViewLogin
{

    /**
     *  Obali data vzhledem stranky a vrati vysledne HTML.
     * @param array $data Data pro zobrazeni.
     * @return string Vysledny vzhled.
     */
    public static function getTemplate($actionResult = null, $isLogged, $name, $role)
    {
        $res = "<div class='col-md-8'>";

        if ($actionResult != null) {
            $res .= "<div class='alert alert-success message' role='alert'>
                        <strong>$actionResult</strong>
                     </div>";
        }

        if (!$isLogged) {
            $res .= "
            <form action='con-index.php?web=login' method='post'>
                <fieldset>
                    <legend>Login</legend>
                    Nick: <input class='login-input1' type='text' name='jmeno' required><br>
                    Heslo: <input class='login-input2' type='password' name='heslo' required><br>
                    <input class='btn btn-success conf-button' type='submit' name='login' value='Login'>
                </fieldset>
            </form>";
        } else {
            $res .= "
            <form action='con-index.php?web=login' method='post'>
                <fieldset>
                    <input class='btn btn-success' type='submit' name='logout' value='Logout'>
                </fieldset>
            </form>";
        }

        $res .= "</div>";


        // doplnim data hlavicky
        include("view-header.class.php");
        include("view-footer.class.php");
        // doplnim hlavicky a vratim
        return ViewHeader::getHTMLHeader("Přihlášení", $isLogged, $name, $role) . $res . ViewFooter::getHTMLFooter();
    }

}

?>
