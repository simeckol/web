<?php

class ModDatabaze {

    private $db; // PDO objekt databaze

    public function __construct() {
        $dbName = "new_schema8";
        // vytvorim objekt PDO pro praci s databazi
        $this->db = new PDO("mysql:host=localhost;dbname=$dbName", "root", "");
    }

    /**
     *  Provede dotaz a buď vrátí jeho výsledek, nebo null a vypíše chybu.
     *  @param string $dotaz    Dotaz.
     *  @return object          Vysledek dotazu.
     */
    private function executeQuery($dotaz){
        // dotaz kvuli diakritice cestiny
        $q = "SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'";
        $result = $this->db->query($q);
        // klasicky dotaz
        $res = $this->db->query($dotaz);
        // zpracovani vysledku dotazu
        if (!$res) {
            $error = $this->db->errorInfo();
            echo $error[2]; // toto by melo byt osetreno lepe !
            return null;
        } else {
            return $res;
        }
    }

    /**
     * Vrací všechny články
     * @return mixed
     */
    public function getAllPosts(){
        $sql = "SELECT * FROM `article`";
        $result = $this->executeQuery($sql);
        return $result->fetchAll();
    }

    /**
     * Vrací všechny uživatele z DB
     * @return mixed
     */
    public function getUsers()
    {
        $sql = "SELECT * FROM `user` JOIN `role` ON `user`.`role`=`role`.`id_role`";
        $result = $this->executeQuery($sql);
        return $result->fetchAll();
    }

    /**
     *  Smaze daneho uzivatele z DB.
     *  @param integer $userId  ID uzivatele
     */
    public function deleteUser($userId){
        $q = "DELETE FROM `user` WHERE `id_user`=$userId";
        $res = $this->executeQuery($q); // provede dotaz
    }

    /**
     * Vrací true pokud je již uživatelské jméno používáno
     * @param $nick nick
     * @return bool
     */
    public function isNickTaken($nick){
        $nick = $this->db->quote($nick);
        $sql = "SELECT * FROM `user` WHERE `nick`=".$nick;
        $user = $this->executeQuery($sql);
        $result = $user->fetchAll();
        if($result->rowCount()>0){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Přidání uživatele do DB
     * @param $nick nick
     * @param $email email
     * @param $pass heslo
     */
    public function addUser($jmeno, $email, $pass){
//        $jmeno= $this->db->quote($jmeno);
//        $email = $this->db->quote($email);
//        $pass = $this->db->quote($pass);

        $sql = "INSERT INTO `user`(`id_user`, `nick`, `password`, `email`, `role`) VALUES (DEFAULT, ?, ?, ?, 2)";
        $res = $this->db->prepare($sql);
        $jmeno = htmlspecialchars($jmeno);
        $email = htmlspecialchars($email);
        $res->execute(array($jmeno, $pass, $email)); // provede dotaz
    }

    /**
     * Vrací pokud uživatel a heslo existují v DB
     * @param $nick nick
     * @param $pass heslo
     * @return mixed
     */
    public function login($log, $pas){
        $sql = "SELECT * FROM `user` JOIN `role` ON `user`.`role`=`role`.`id_role` WHERE `nick`=".$log;
        $user = $this->executeQuery($sql);
        if($user !=null){
            $_SESSION["prihlasen"]=$user; // zahajim session uzivatele
            return "ANO";
        } else {
            return "NE";
        }
    }

    /**
     * Vrací informace o uživateli
     * @param $id id uživatele
     * @return mixed
     */
    public function getUser($log, $pas){

        $log = $this->db->quote($log);
        $pas = $this->db->quote($pas);

        $sql = "SELECT * FROM `user` JOIN `role` ON `user`.`role`=`role`.`id_role` WHERE `nick`=".$log." AND `password`=".$pas;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací informace o uživateli
     * @param $id id uživatele
     * @return mixed
     */
    public function getUserInfo($id) {

        $sql = "SELECT * FROM `user` WHERE `id_user`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Přidání příspěvku do DB
     * @param $title titulek
     * @param $autors string
     * @param $abstract obsah
     * @param $id_author idautora
     * @param $date datum
     */
    public function addPost($title, $autors, $abstract, $id_author, $date){
        $title = $this->db->quote($title);
        $autors = $this->db->quote($autors);
        $abstract = $this->db->quote($abstract);
        $id_author = $this->db->quote($id_author);
        $date = $this->db->quote($date);

        $sql = "INSERT INTO `article`(`id`, `title`, `autors`, `content`,`id_post_author`,`date`,`accepted`) VALUES(DEFAULT," . $title . "," . $autors . "," . $abstract . "," . $id_author . ",".$date.",0)";

        $query = $this->db->prepare($sql);
        $query->execute();
    }


    /**
     * Vrací všchny posty uživatele
     * @param $id id uživatele
     * @return mixed
     */
    public function getPostsByUser($id){
        $id = $this->db->quote($id);
        $sql = "SELECT * FROM `article` WHERE `id_post_author` =".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací všchny posty uživatele
     * @param $id id uživatele
     * @return mixed
     */
    public function getPostsById($id){
        $sql = "SELECT * FROM `article` WHERE `id` =".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací posty s hodnocením
     * @param $id id uživatele
     * @return mixed
     */
    public function getPostsWithReviews(){
        $sql = "SELECT * FROM article LEFT OUTER JOIN reviews ON article.id = reviews.id_article;";
        $res = $this->executeQuery($sql); // provede dotaz
        return $res->fetchAll();
    }

    /**
     * Vrací všchny hodnocení článku
     * @param $id id článku
     * @return mixed
     */
    public function getReviewsByArticle($id){
        $id = $this->db->quote($id);
        $sql = "SELECT * FROM `reviews` WHERE `id_article` =".$id." AND `idea` != 0";
        $res = $this->executeQuery($sql); // provede dotaz
        return $res->fetchAll();
    }

    /**
     * Smaže příspěvek
     * @param $id id postu
     */
    public function deletePost($id){
        $id = $this->db->quote($id);
        $sql = "DELETE FROM `article` WHERE `id`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Upravení příspěvku
     * @param $id idpostu
     * @param $abstract obsah
     */
    public function editPost($id, $abstract){

        $abstract = $this->db->quote($abstract);
        $id = $this->db->quote($id);

        $sql = "UPDATE `article` SET `content`= ".$abstract." WHERE `id`= ".$id;
        $res = $this->executeQuery($sql); // provede dotaz
    }

    /**
     * Všechny hodnocení uživatele
     * @param $id uživatele
     * @return mixed
     */
    public function getUserAssignedPosts($id){
        $sql = "SELECT * FROM `article` JOIN `reviews` ON `article`.`id`=`reviews`.`id_article` JOIN `user` ON `reviews`.`id_reviewer`=`user`.`id_user` WHERE `reviews`.`id_reviewer`=".$id." ORDER BY `reviews`.`id_article`";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Upraví informace o uživateli
     * @param $id id uživatele
     * @param $role role
     */
    public function editUser($id, $role){

        $sql = "UPDATE `user` SET `role`=".$role." WHERE `id_user`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Ohodnocení postu
     * @param $id id článku
     * @param $reviewer id hodnotitele
     * @param $idea nápad
     * @param $theme téma
     * @param $note poznámka
     */
    public function reviewPost($id, $reviewer,$idea, $theme, $note){
        $note = $this->db->quote($note);
        $sql = "UPDATE `reviews` SET `idea`=".$idea .",`theme`=".$theme.",`note`=".$note." WHERE `id_reviewer`=".$reviewer." AND `id_article`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Ohodnocení postu
     * @param $id id článku
     * @param $reviewer id hodnotitele
     * @param $idea nápad
     */
    public function reviewPostByIdea($id, $reviewer, $idea){
        $sql = "UPDATE `reviews` SET `idea`=".$idea." WHERE `id_reviewer`=".$reviewer." AND `id_article`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Ohodnocení postu
     * @param $id id článku
     * @param $reviewer id hodnotitele
     * @param $theme téma
     */
    public function reviewPostByTheme($id, $reviewer, $theme){
        $sql = "UPDATE `reviews` SET `theme`=".$theme." WHERE `id_reviewer`=".$reviewer." AND `id_article`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Ohodnocení postu
     * @param $id id článku
     * @param $reviewer id hodnotitele
     * @param $note poznámka
     */
    public function reviewPostByNote($id, $reviewer, $note){
        $note = $this->db->quote($note);
        $sql = "UPDATE `reviews` SET `note`=".$note." WHERE `id_reviewer`=".$reviewer." AND `id_article`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }


    /**
     * Vrací hodnocení postu
     * @param $id id postu
     * @param $reviewer id hodnotitele
     * @return mixed
     */
    public function getPostReview($id, $reviewer){
        $sql = "SELECT * FROM `reviews` WHERE `id_article`=".$id." AND `id_reviewer`=".$reviewer;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací scvhálené články
     * @return mixed
     */
    public function getAcceptedPosts(){
        $sql = "SELECT * FROM `article` WHERE `accepted`=1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací nescvhálené články
     * @return mixed
     */
    public function getUnacceptedPosts(){
        $sql = "SELECT * FROM `article` WHERE `accepted`=0";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Schválení postu
     * @param $id id postu
     */
    public function approvePost($id){
        $sql = "UPDATE `article` SET `accepted`=1 WHERE `id`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Zamitnuti postu
     * @param $id id postu
     */
    public function disapprovePost($id){
        $sql = "UPDATE `article` SET `accepted`= -1 WHERE `id`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Uzamčení hodnocení pro všechny hodnotitele článku
     * @param $id id článku
     */
    public function lockPost($id){
        $sql = "UPDATE `reviews` SET `lock_edit`=1 WHERE `id_article`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Vrací všechny uživatele s rolí recenzenta
     * @return mixed
     */
    public function getReviewers(){
        $sql = "SELECT * FROM `user` WHERE `role`=3";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * @return mixed
     */
    public function getReviewersOfArticle($id){
        $sql = "SELECT user.id_user, user.nick FROM `user` LEFT OUTER JOIN reviews ON user.id_user = reviews.id_reviewer WHERE `id_article`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací true pokud uživatel již post nehodnotí
     * @param $id_reviewer id uživatele
     * @param $id_post id článku
     * @return bool
     */
    public function canReview($id_reviewer, $id_post)
    {
        $sql = "SELECT * FROM `reviews` WHERE `id_article`=" . $id_post . " AND `id_reviewer`=" . $id_reviewer;
        $query = $this->db->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 0) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Přiřadí hodnocení příspěvku uživateli
     * @param $id_article id postu
     * @param $id_reviewer id uživatele
     */
    public function assignPost($id_article, $id_reviewer){
        $sql = "INSERT INTO `reviews`(`id_review`, `id_article`, `id_reviewer`, `idea`, `theme`, `note`, `lock_edit`) VALUES (DEFAULT,".$id_article.",".$id_reviewer.",0,0,'',0)";
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     *  Smaze pritazeni hodnoceni
     * @param $id_article id postu
     * @param $id_reviewer id uživatele
     */
    public function deleteAssignment($id_article, $id_reviewer){
        $q = "DELETE FROM `reviews` WHERE `id_article`= ".$id_article." AND `id_reviewer` = ".$id_reviewer;
        $res = $this->executeQuery($q); // provede dotaz
    }

    /**
     * Vrací true pokud je již uživatelské jméno používáno
     * @param $nick nick
     * @return bool
     */
    public function isReviewAssigned($id_article, $id_reviewer){
        $sql = "SELECT * FROM `reviews` WHERE `id_article`= ".$id_article." AND `id_reviewer` = ".$id_reviewer;

        $res = $this->executeQuery($sql);
        if ($res == null) {
            return false;
        }


        if($res->rowCount()>0){
            return true;
        }
        else{
            return false;
        }
    }

}

?>